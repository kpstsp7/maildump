#!/usr/bin/env python
#-*- coding:utf-8 -*-

from email.utils import getaddresses
from email.header import decode_header, Header, make_header


import imaplib
import getpass
import os
import logging
import logging.config
import email
import re
import poplib


#DONE: check subfolders if need (cheked ALL without folders)
#TODO: check for attachment
#TODO: check for multipart
#DONE: save with email subject in log
#TODO: add processing different maildir (by circrle)
#DONE: add not't touch feature = done for imap
"""
The POP protocol has no concept of "read" or "unread" messages; the LIST
command simply shows all existing messages. You may want to use another
protocol, like IMAP, if the server supports it.
"""
#DONE: add pop3 processing

logging.config.fileConfig(os.path.join(os.path.abspath("."), 'logger.conf'))
# create logger
logger = logging.getLogger('simpleLogger')


def getheader(header_text, default="ascii"):
    """Decode the specified header"""

    headers = decode_header(header_text)
    header_sections = [unicode(text, charset or default)
                       for text, charset in headers]
    return u"".join(header_sections)

def dcode(str):
    h = email.Header.Header(str)
    dh = email.Header.decode_header(h)
    return dh[0][0]


def imapclones(rhost, username, remote_folder, local_folder, password, protocol):

    logger.debug("Start backing up %s to %s", username, local_folder)
    logger.debug("Get email by %s protocol", protocol)
    if (protocol=='imap'):
        mail = imaplib.IMAP4_SSL(rhost)
        mail.login(username, password)
        mail.select(remote_folder,  readonly=True)
        result, data = mail.uid('search', None, 'ALL')
        messages = data[0].split()
        for message_uid in messages:
            typ, data = mail.uid('fetch', message_uid, '(RFC822)')
            #get subject
            raw_email = data[0][1]
#.decode('utf-8', errors='ignore')
            msg = email.message_from_string(raw_email)
            gSub=msg["Subject"]
#.decode('utf-8', errors='ignore')
            if gSub == None:
                subject = "Empty"
            elif(len(gSub)>0):
                subject=getheader(gSub)
            else:
                subject = "Empty2"
            logger.debug("Save %s : uid_mail from account: %s and Subject: %s ", message_uid, username, subject)
            f = open('%s/%s.eml' %(local_folder, message_uid), 'w')
            print >> f, data[0][1]
        mail.close()
        mail.logout()
    else:
        defport="110"
        mailpop=poplib.POP3_SSL(rhost)
        mailpop.user(username)
        mailpop.pass_(password)
        num = len(mailpop.list()[1])

        for msg_id in range(num):

            text = mailpop.retr(msg_id+1)[1]
            text = '\n'.join(text)
            amail = email.message_from_string(text)
            subject = dcode(amail.get("subject"))
            logger.debug("Save %s uid from account: %s with subject: %s",msg_id ,username, subject)
            fout = open('%s/%s.eml' %(local_folder, msg_id), 'w')
            fout.write('\n'.join(mailpop.retr(msg_id+1)[1]))
            fout.close()
        mailpop.quit()



