#!/usr/bin/env python
#-*- coding:utf-8 -*-
import imapclone
import ConfigParser
import csv
import re
import os
from multiprocessing import  Pool
from multiprocessing.dummy import Pool as ThreadPool
import subprocess
import logging
import logging.config
from itertools import chain


tpool = ThreadPool(4)

config=ConfigParser.ConfigParser()

logging.config.fileConfig(os.path.join(os.path.abspath("."), 'loggervpn.conf'))
logger = logger.getLogger('simpleLogger')

#TODO: processing config exception
config.read('settings.cfg')
backuproot=config.get('main', 'backup_root')
#TODO compress folder
compress=config.get('main', 'compress')
usevpn=config.get('main', 'usevpn')
#backup mode - simulation (0) or real backup (1)
backupmode=config.get('main','backupmode')
loglevel=config.get('main','loglevel')
addresses=config.get('main','addressfile')


csv.register_dialect('addr', delimiter=';', quoting=csv.QUOTE_NONE)

reader = csv.DictReader(open(addresses), dialect="addr")

pool = {}

for row in reader:
    for column, value in row.iteritems():
        pool.setdefault(column, []).append(value)

uniq_country = (pool['country'])
#next for each country create and processing pool
n=0
protocol='pop'
#TODO: add country not found exception
#DONE: get imap server by email
for k in pool['address']:
    login=k
    basedomain=re.split("@",k)[1]
    if (pool['protocol'][n]=='imap'):
        srvname="imap."+basedomain
        protocol='imap'
    else:
        srvname="pop."+basedomain
    passw = pool['pass'][n]
    n=n+1
    #debug
    print srvname
    print passw
    if not (os.path.exists(backuproot+'/'+k)):
        os.mkdir(backuproot+'/'+k)
    localfolder=backuproot+'/'+k
    imapclone.imapclones(srvname, k, 'INBOX', localfolder, passw, protocol)







